# WP \_UFM

## Wordpress theme \_Underscores Foundation Laravel Mix starter boilerplace

This is a 'quickstart' boilerplate to help spin up faster development for a new wordpress theme.

The theme structure and PHP is based upon \_s (underscores), using Foundation 6 for SASS/CSS and a few JS components (if required). Laraval Mix is used to simplify the Webpack build process which includes SASS processing, JS compiling (ES5) & browsersync.

This boilerplate is still in development.

## Pre-requisits

-   Exisiting local dev envronment (docker, flywheel, Valet, Valet Plus (https://github.com/weprovide/valet-plus) )
-   NPM & Composer are both installed and up to date.

## Set up instructions

1.   Spin up you local dev enviroment. Personally I currently favour Valet Plus (for the server & DB) along with Roots Bedrock for the WP envrionment.
2.  Clone this repo into your Wordpress themes folder
3.  Find and replace any instances of `WP_UFM` withing the theme files with the name for your new theme or project (i.e. `my-awesome-theme`).
4.   Replace `test.test` with the URL of your local wordpress environment (to ensure browsersyn works as expexted).
5.   From within the project directory install dependancies using the following steps.

```
npm install
```

followed by:

```
composer install
```

Once all the dependancies have been installed start the dev build process using

```
npm run dev
```

## Workflow

Edit the JS and SASS files within the SRC folder. Only code and any imports within these two files will be processed. Therefore if you are using multiple SASS or JS files ensure they are imported correctly within app.js and app.scss

Editing any file, including PHP files within the `theme` folder, will result in a browser refresh.

Foundation is imported from within the node_modules folder. 
Style settings can be updated and changed within `src/scss/foundation` within the theme SRC folder.
JS components cab be added or removed (commented-out) by editing `src/js/_foundation-explicit-pieces.js`


## Linting and formatting code within VS Code

My code editor of choice is currently VS Code. https://code.visualstudio.com
There are also files within the repo to help style, lint and format code within VSCode.
Please see https://sridharkatakam.com/linting-and-formatting-in-visual-studio-code-for-wordpress/ for more details.
If you've got this far, then steps 1, 2 & 3 of that guide are already complete.

## To do

-   Better error reporting
-   Refining the whole package
-   Testing Foundation JS imports (for components such as Exchange and Equalize)
-   Maybe adding further JS compnents to node_modules (such as featherlight, Slick slide etc)
