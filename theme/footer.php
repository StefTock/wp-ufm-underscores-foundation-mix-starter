<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_UFM
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'WP_UFM' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'WP_UFM' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'WP_UFM' ), 'WP_UFM', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<div id="engadget-footer-sitemap-container">
  <footer id="engadget-footer-sitemap">
    <div class="links">
      <div class="link-column">
        <ul class="menu vertical">
          <li><a href="#">One</a></li>
          <li><a href="#">Two</a></li>
          <li><a href="#">Three</a></li>
          <li><a href="#">Four</a></li>
        </ul>
      </div>
      <div class="link-column">
        <ul class="menu vertical">
          <li><a href="#">One</a></li>
          <li><a href="#">Two</a></li>
          <li><a href="#">Three</a></li>
          <li><a href="#">Four</a></li>
        </ul>
      </div>
       <div class="link-column">
        <ul class="menu vertical">
          <li><a href="#">One</a></li>
          <li><a href="#">Two</a></li>
          <li><a href="#">Three</a></li>
          <li><a href="#">Four</a></li>
        </ul>
      </div>
      <div class="link-column">
        <ul class="menu vertical">
          <li><a href="#">One</a></li>
          <li><a href="#">Two</a></li>
          <li><a href="#">Three</a></li>
          <li><a href="#">Four</a></li>
        </ul>
      </div>
    </div>
  </footer>
</div>
<div id="engadget-footer-social-container">
    <footer id="engadget-footer-social">
        <div class="footer-left">
            <div class="newsletter">
                <h6>Sign up for our newsletter</h6>
                <div class="input-group">

                    <input class="input-group-field" type="email" placeholder="Email address">
                    <div class="input-group-button">
                        <input type="submit" class="button" value="Submit">
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-right">
            <h6>Follow us</h6>
            <i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i>
            <i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i>
            <i class="fa fa-google-plus-square fa-3x" aria-hidden="true"></i>
            <i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i>
        </div>
    </footer>
</div>
<div id="engadget-footer-contact-details-container">
    <footer id="engadget-footer-contact-details">
        <div class="footer-left">
            <div class="contact-details">
                <ul>
                    <li><img class="thumbnail" src="http://placehold.it/80"></li>
                    <li><i class="fa fa-phone fa-lg" aria-hidden="true"></i> 01234 567890</li>
                    <li><a data-toggle="animatedModal10"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact us</a></li>
                    <li><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> Street, City, County, Country</li>
                </ul>
            </div>
        </div>
        <div class="footer-right">
            <ul class="menu align-right">
                <li><a href="#">One</a></li>
                <li><a href="#">Two</a></li>
                <li><a href="#">Three</a></li>
                <li><a href="#">Four</a></li>
            </ul>
        </div>
    </footer>
</div>



</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
